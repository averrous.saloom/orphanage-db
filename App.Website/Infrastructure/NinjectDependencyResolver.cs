﻿using App.Data;
using App.Data.Entities;
using App.Website.App_Start;
using App.Website.Services;
using App.Website.Services.Contracts;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Moq;
using Ninject;
using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace App.Website.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel Kernel;
        public NinjectDependencyResolver(IKernel kernel)
        {
            Kernel = kernel;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return Kernel.TryGet(serviceType);        
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return Kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            Kernel.Bind<ApplicationDbContext>().ToSelf().InSingletonScope();


            Kernel.Bind<IOrphanageService>().To<OrphanageService>().InSingletonScope();
            //Kernel.Bind<IOrphanageService>().ToConstant(orphanageServiceMock.Object);
            Kernel.Bind<ApplicationUserManager>().ToMethod(ctx => GetUserManagerFromOwinContext())
                .InRequestScope();
            Kernel.Bind<ApplicationSignInManager>().ToMethod(ctx => GetSignInManagerFromOwinContext())
                .InRequestScope();
            Kernel.Bind<ApplicationRoleManager>().ToMethod(ctx => GetRoleManagerFromOwinContext())
                .InRequestScope();
        }

        public static ApplicationUserManager GetUserManagerFromOwinContext()
        {
            return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
        }

        public static ApplicationSignInManager GetSignInManagerFromOwinContext()
        {
            return HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
        }

        public static ApplicationRoleManager GetRoleManagerFromOwinContext()
        {
            return HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
        }

    }
}