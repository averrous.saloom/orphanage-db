﻿using App.Data;
using App.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Website.Services.Contracts
{
    public interface IService
    {
        List<T> GetAll<T>() where T: EntityBase;
        T Upsert<TViewModel, T>(TViewModel model) where T : EntityBase, new();
        bool Delete<T>(string id) where T : EntityBase;
        T GetById<T>(string id) where T : EntityBase;
    }
}
