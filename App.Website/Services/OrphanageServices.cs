﻿using App.Data;
using App.Data.Entities;
using App.Website.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Website.Services
{
    public class OrphanageService : ServiceBase, IOrphanageService
    {
        public OrphanageService(ApplicationDbContext db): base(db) { }
    }
}