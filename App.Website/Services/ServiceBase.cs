﻿using App.Data;
using App.Website.Services.Contracts;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace App.Website.Services
{
    public class ServiceBase : IService
    {
        ApplicationDbContext db;
        public ServiceBase(ApplicationDbContext db) {
            this.db = db;
        }

        public bool Delete<T>(string id) where T : EntityBase
        {
            var entity = db.Set<T>().Where(t => t.Id == id).FirstOrDefault();
            if (entity == null)
            {
                return false;
            }

            entity.IsDeleted = true;
            db.Set<T>().AddOrUpdate(entity);
            db.SaveChanges();
            return true;
        }

        public List<T> GetAll<T>() where T : EntityBase
        {
            return db.Set<T>().Where(t => !t.IsDeleted).ToList();
        }

        public T GetById<T>(string id) where T : EntityBase
        {
            return db.Set<T>().Where(t =>t.Id == id).FirstOrDefault();
        }

        public T Upsert<TViewModel, T>(TViewModel model) where T : EntityBase, new()
        {
            var entity = new T();
            entity = Mapper.Map(model, entity);
            if (entity.Id != null)
            {
                db.Set<T>().AddOrUpdate(entity);
            } else
            {
                entity.Init();
                entity = db.Set<T>().Add(entity);
            }
            db.SaveChanges();
            return entity;
        }
    }
}