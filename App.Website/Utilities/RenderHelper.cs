﻿using System;
using System.Text;
using System.Web.Mvc;

namespace App.Website.Utilities
{
    public static class RenderHelper
    {
        public static MvcHtmlString RenderProperty<T>(this HtmlHelper<T> html, T model, string property)
        {
            StringBuilder result = new StringBuilder("");
            var prop = model.GetType().GetProperty(property);
            if (prop == null)
            {
                result.Append("Not Found");
            } else
            {
                result.Append(prop.GetValue(model).ToString());
            }
            return new MvcHtmlString(result.ToString());
        }
        public static MvcHtmlString RenderColumnsHtml<T>(this HtmlHelper<T> html, Type tableModel)
        {
            StringBuilder result = new StringBuilder("");

            return new MvcHtmlString(result.ToString());
        }
    }
}