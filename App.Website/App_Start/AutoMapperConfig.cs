﻿using App.Data.Entities;
using App.Website.Models;
using AutoMapper;

namespace App.Web
{
    public class AutoMapperConfig
    {
        public static void InitMapper()
        {
            Mapper.Initialize(RegisterMapper);
        }

        private static void RegisterMapper(IMapperConfigurationExpression cfg)
        { 
            cfg.CreateMap<Orphanage, OrphanageViewModel>().ReverseMap()
                .ForMember(t => t.Addresses, opt => opt.Ignore())
                .ForMember(t => t.Socials, opt => opt.Ignore())
                .ForMember(t => t.Needs, opt => opt.Ignore())
                .ForMember(t => t.Medias, opt => opt.Ignore());
        }
    }
}