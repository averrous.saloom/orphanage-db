﻿using System.ComponentModel.DataAnnotations;

namespace App.Website.Models
{
    public class OrphanageViewModel
    {
        [Key]
        [ScaffoldColumn(false)]
        public string Id { get; set; }
        public string Name { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [DataType(DataType.MultilineText)]
        public string About { get; set; }
        [DataType(DataType.MultilineText)]
        public string Mission { get; set; }
        [DataType(DataType.MultilineText)]
        public string Vision { get; set; }
        [DataType(DataType.MultilineText)]
        public string History { get; set; }
    }
}