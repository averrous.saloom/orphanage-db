﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace App.Website.Models
{
    public class LoginViewModel
    {
        [DisplayName("Username or Email")]
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}