﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Data.Entities;
using App.Data.Identity;
using App.Website.Models;
using App.Website.Services.Contracts;
using App.Website.Utilities;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace App.Website.Controllers
{
    public class HomeController : Controller
    {
        private string[] COLUMNS = new string[]
        {
            "Name",
            "Description",
            "History",
            "Vision",
            "Mission",
        };
        private IOrphanageService orphanageService;
        public HomeController(IOrphanageService orphanageService) 
        { 
            this.orphanageService = orphanageService;
        }
        public ActionResult Index()
        {
            var model = orphanageService.GetAll<Orphanage>();
            ViewBag.Columns = COLUMNS;
            return View(model);
        }

        [Authorize]
        public ActionResult Add()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Add(OrphanageViewModel viewModel)
        {
            var entity = orphanageService.Upsert<OrphanageViewModel, Orphanage>(viewModel);
            if (entity != null)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Add");
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            orphanageService.Delete<Orphanage>(Id);
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Update(string Id)
        {
            var entity = orphanageService.GetById<Orphanage>(Id);
            var viewModel = new OrphanageViewModel();
            viewModel = Mapper.Map(entity, viewModel);
            return View(viewModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Update(OrphanageViewModel viewModel)
        {
            var entity = orphanageService.Upsert<OrphanageViewModel, Orphanage>(viewModel);
            if (entity != null)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Update");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}