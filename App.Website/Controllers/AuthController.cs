﻿using App.Data.Identity;
using App.Website.App_Start;
using App.Website.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace App.Website.Controllers
{
    public class AuthController : Controller
    {
        private ApplicationUserManager applicationUserManager;
        private ApplicationSignInManager applicationSignInManager;
        public AuthController(ApplicationUserManager applicationUserManager, ApplicationSignInManager applicationSignInManager = null)
        {
            this.applicationUserManager = applicationUserManager;
            this.applicationSignInManager = applicationSignInManager;
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel registerViewModel)
        {
            var user = new ApplicationUser() { UserName = registerViewModel.UserName, Email = registerViewModel.Email };
            user.Init();
            IdentityResult result = applicationUserManager.Create(user, registerViewModel.Password);
            
            if (result.Succeeded)
            {
                var identity = applicationUserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                applicationSignInManager.AuthenticationManager.SignIn(new AuthenticationProperties() { }, identity);
                return RedirectToAction("Index", "Home");
            }

            return RedirectToAction("Index", "Auth");
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginViewModel)
        {
            var user = applicationUserManager.Find(loginViewModel.UserId, loginViewModel.Password);
            if (user == null) { 
                return RedirectToAction("Login", "Auth");
            }

            var identity = applicationUserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            applicationSignInManager.AuthenticationManager.SignIn(new AuthenticationProperties() { }, identity);
            return RedirectToAction("Index", "Home");
        }
    }
}