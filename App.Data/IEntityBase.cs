﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace App.Data
{
    public interface IEntityBase
    {
        [Key]
        [Required]
        [MaxLength(128)]
        string Id { get; set; }
        string CreatedBy { get; set; }

        DateTime CreatedDate { get; set; }
        string UpdatedBy { get; set; }
        DateTime? UpdatedDate { get; set; }
        [DefaultValue(false)]
        bool IsDeleted { get; set; }
        [DefaultValue(false)]
        bool IsDraft { get; set; }
        void Init();
        void Delete(string userName);
    }
}
