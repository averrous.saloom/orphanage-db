﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Data.Entities
{
    public class Orphanage : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string About { get; set; }
        public string Mission { get; set; }
        public string Vision { get; set; }
        public string History { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<Social> Socials { get; set; }
        public virtual ICollection<Media> Medias { get; set; }
        public virtual ICollection<Need> Needs { get; set; }
    }
    public class Media : EntityBase
    {
        public string Title { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }
        public string MediaType { get; set; }
        public string Description { get; set; }
        [DefaultValue(false)]
        public string IsValidated { get; set; }
        public Orphanage Orphanage { get; set; }
    }
    public class Address : EntityBase
    {
        public string Title { get; set; }
        public string FullAddress { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string KelurahanDesa { get; set; }
        public string Kecamatan { get; set; }
        public string KotaKabupaten { get; set; }
        public string Provinsi { get; set; }
        public string PostNumber { get; set; }
        public bool IsValidated { get; set; }
        public Orphanage Orphanage { get; set; }
    }
    public class Social : EntityBase
    {
        public string Platform { get; set; }
        public string Type { get; set; }
        public string Handler { get; set; }
        public string Link { get; set; }
        [DefaultValue(false)]
        public bool IsValidated { get; set; }
        public string ActiveRate { get; set; }
        public Orphanage Orphanage { get; set; }
    }
    public class Need : EntityBase
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int weights { get; set; }
        [DefaultValue(false)]
        public bool IsValidated { get; set; }
        public Orphanage Orphanage { get; set; }
    }
}
