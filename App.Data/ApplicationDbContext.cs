﻿using App.Data.Entities;
using App.Data.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>
    {
        public ApplicationDbContext() : base(nameOrConnectionString: "DefaultConnection") { }
        public ApplicationDbContext(string connStr) : base(nameOrConnectionString: connStr) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<ApplicationDbContext>(null);
            modelBuilder.Properties<decimal>().Configure(c => c.HasPrecision(21, 3));
            base.OnModelCreating(modelBuilder);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Orphanage> Ophanages { get; set; }
        public DbSet<Social> Socials { get; set; }
        public DbSet<Media> Medias { get; set; }
        public DbSet<Need> Needs { get; set; }        
    }
}
