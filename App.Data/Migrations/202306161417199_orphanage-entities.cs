﻿namespace App.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orphanageentities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Media",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Title = c.String(),
                        FilePath = c.String(),
                        FileType = c.String(),
                        MediaType = c.String(),
                        Description = c.String(),
                        IsValidated = c.String(),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsDraft = c.Boolean(nullable: false),
                        Orphanage_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orphanages", t => t.Orphanage_Id)
                .Index(t => t.Orphanage_Id);
            
            CreateTable(
                "dbo.Orphanages",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Description = c.String(),
                        About = c.String(),
                        Mission = c.String(),
                        Vision = c.String(),
                        History = c.String(),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsDraft = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Title = c.String(),
                        FullAddress = c.String(),
                        Longitude = c.String(),
                        Latitude = c.String(),
                        KelurahanDesa = c.String(),
                        Kecamatan = c.String(),
                        KotaKabupaten = c.String(),
                        Provinsi = c.String(),
                        PostNumber = c.String(),
                        IsValidated = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsDraft = c.Boolean(nullable: false),
                        Orphanage_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orphanages", t => t.Orphanage_Id)
                .Index(t => t.Orphanage_Id);
            
            CreateTable(
                "dbo.Needs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Title = c.String(),
                        Description = c.String(),
                        weights = c.Int(nullable: false),
                        IsValidated = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsDraft = c.Boolean(nullable: false),
                        Orphanage_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orphanages", t => t.Orphanage_Id)
                .Index(t => t.Orphanage_Id);
            
            CreateTable(
                "dbo.Socials",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Platform = c.String(),
                        Type = c.String(),
                        Handler = c.String(),
                        Link = c.String(),
                        IsValidated = c.Boolean(nullable: false),
                        ActiveRate = c.String(),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsDraft = c.Boolean(nullable: false),
                        Orphanage_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orphanages", t => t.Orphanage_Id)
                .Index(t => t.Orphanage_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Socials", "Orphanage_Id", "dbo.Orphanages");
            DropForeignKey("dbo.Needs", "Orphanage_Id", "dbo.Orphanages");
            DropForeignKey("dbo.Media", "Orphanage_Id", "dbo.Orphanages");
            DropForeignKey("dbo.Addresses", "Orphanage_Id", "dbo.Orphanages");
            DropIndex("dbo.Socials", new[] { "Orphanage_Id" });
            DropIndex("dbo.Needs", new[] { "Orphanage_Id" });
            DropIndex("dbo.Addresses", new[] { "Orphanage_Id" });
            DropIndex("dbo.Media", new[] { "Orphanage_Id" });
            DropTable("dbo.Socials");
            DropTable("dbo.Needs");
            DropTable("dbo.Addresses");
            DropTable("dbo.Orphanages");
            DropTable("dbo.Media");
        }
    }
}
