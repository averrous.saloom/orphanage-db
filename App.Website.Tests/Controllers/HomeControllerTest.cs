﻿using App.Data.Entities;
using App.Website;
using App.Website.Controllers;
using App.Website.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace App.Website.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            Mock<IOrphanageService> orphanageServiceMock = new Mock<IOrphanageService>();
            orphanageServiceMock.Setup(m => m.GetAll<Orphanage>()).Returns(new List<Orphanage>());
            // Arrange
            HomeController controller = new HomeController(orphanageServiceMock.Object);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            Mock<IOrphanageService> orphanageServiceMock = new Mock<IOrphanageService>();
            orphanageServiceMock.Setup(m => m.GetAll<Orphanage>()).Returns(new List<Orphanage>());
            // Arrange
            HomeController controller = new HomeController(orphanageServiceMock.Object);

            // Act
            ViewResult result = controller.Add() as ViewResult;

            // Assert
            Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            Mock<IOrphanageService> orphanageServiceMock = new Mock<IOrphanageService>();
            orphanageServiceMock.Setup(m => m.GetAll<Orphanage>()).Returns(new List<Orphanage>());
            // Arrange
            HomeController controller = new HomeController(orphanageServiceMock.Object);

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
